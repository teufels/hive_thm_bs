########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_thm_bs/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[request.getNormalizedParams() && like(request.getNormalizedParams().getHttpHost(), "/development.*/")]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_thm_bs/Configuration/TypoScript/Constants/Development" extensions="txt">
[END]